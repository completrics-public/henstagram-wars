﻿### Name

Arthur Chickenson

### Aspects

#### Body

* good looks
* nice voice
* strong like an ox

#### Henhouse

* sturdy construction

#### Hen

* beautiful
* well-fed
* awesome voice
* astonishing talent
* lovely
