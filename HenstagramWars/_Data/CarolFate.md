﻿### Name

Carol Fate

### Aspects

#### Body

* well-connected
* perfect engineer

#### Henhouse

* huge
* triple protection from foxes and cats
* looks amazing
* an actor helped build it
* artistically done

#### Hen

* a group of hens, nothing more
