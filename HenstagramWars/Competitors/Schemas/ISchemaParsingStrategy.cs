﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HenstagramWars.Competitors.Schemas
{
    public interface ISchemaParsingStrategy
    {
        Competitor Parse(string textToParse);
        double CalculateFitness(string textToParse);
    }
}
