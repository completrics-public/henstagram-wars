﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HenstagramWars.Competitors.Schemas
{
    public class NullObjectParsingStrategy : ISchemaParsingStrategy
    {
        public double CalculateFitness(string ct)
        {
            return 0;
        }

        public Competitor Parse(string textToParse)
        {
            return null;
        }
    }
}
