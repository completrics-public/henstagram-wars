﻿using HenstagramWars.Competitors;
using HenstagramWars.Contests;
using HenstagramWars.Contests.Comparators;
using HenstagramWars.Scores;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace HenstagramWars
{
    class Program
    {
        static void Main(string[] args)
        {
            // Configuration
            eStrategy competitorComparisonStrategy = eStrategy.RandomlessSum;

            // Simple example how strategy + factory method work:
            IComparisonStrategy selectedStrategy = ComparisonStrategyFactory.Create(competitorComparisonStrategy);

            // More complex example (similar to the livetalk one) how strategy + autoconfigured factory method work:
            List<Competitor> competitors = new CompetitorFactory().BuildCompetitors();

            // Just normal code things, also, internally a potential for one MORE strategy.
            Score score = new Contest().Run(competitors, selectedStrategy);

            Console.WriteLine(score.ToString());
            Console.ReadLine();
        }
    }
}
