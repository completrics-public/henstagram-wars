﻿using HenstagramWars.Competitors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HenstagramWars.Scores
{
    public class ScoreFactory
    {
        /// <summary>
        /// Responsible for a PROPER SETUP of a Score class. Including sorting.
        /// So this particular factory is responsible for setting stuff up, not building one of many.
        /// </summary>
        public static Score Create(IList<Competitor> winners)
        {
            var result = winners.GroupBy(w => w.Name);

            List<PersonalScore> personalScores = new List<PersonalScore>();
            foreach (var r in result)
            {
                personalScores.Add(new PersonalScore(r.Key, r.Count()));
            }

            // This sorting here means the HIGHER the score the CLOSER TO THE TOP we are.
            personalScores.Sort((s1, s2) => s2.Score.CompareTo(s1.Score));

            return new Score(personalScores);
        }
    }
}
