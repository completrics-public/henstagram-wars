﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HenstagramWars.Scores
{
    /// <summary>
    /// Score for many competitors. As in, collective aggregate of all competitors' scores.
    /// The higher to the top you are, the higher PersonalScore you have.
    /// Btw, thanks to a Factory - properly initialized.
    /// </summary>
    public class Score
    {

        public Score(IList<PersonalScore> personalScores)
        {
            AllCompetitors = personalScores;
        }

        public IList<PersonalScore> AllCompetitors { get; }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            if(AllCompetitors.Count > 0)
                sb.Append($"First place: ({AllCompetitors[0].Name}) with score ({AllCompetitors[0].Score})\n");
            if (AllCompetitors.Count > 1)
                sb.Append($"Second place: ({AllCompetitors[1].Name}) with score ({AllCompetitors[1].Score})\n");
            if (AllCompetitors.Count > 2)
                sb.Append($"Third place: ({AllCompetitors[2].Name}) with score ({AllCompetitors[2].Score})\n");
            if (AllCompetitors.Count > 3)
                sb.Append("And no other place matters at the bloody court of opinion at Henstagram...");

            return sb.ToString();
        }
    }

    /// <summary>
    /// Score of a single competitor - only a single person is located here.
    /// </summary>
    public class PersonalScore
    {
        public PersonalScore(string name, int score)
        {
            Name = name;
            Score = score;
        }

        public string Name { get; }
        public int Score { get; }
    }
}
