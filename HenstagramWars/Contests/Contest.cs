﻿using HenstagramWars.Competitors;
using HenstagramWars.Contests.Comparators;
using HenstagramWars.Scores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HenstagramWars.Contests
{
    public class Contest
    {
        public Score Run(IList<Competitor> competitors, IComparisonStrategy comparison)
        {
            // We are dealing with "everyone competes with everyone" scheme.
            // Double loop is funny, because you compare everyone with everyone TWICE.
            // (yes, including oneself).

            // YES, this thing below could have been a strategy (contest running strategy) 
            // injected as a parameter in the constructor in such a way:
            // public Score Run(IList<Competitor> competitors, IComparisonStrategy comparison, IContestRunningStrat crs)
            // then it could have been properly built by a factory one level above like comparison strats are built.

            IList<Competitor> winners = new List<Competitor>();

            for (int i=0; i<competitors.Count(); i++)
            {
                for (int j=0; j<competitors.Count(); j++)
                {
                    Competitor winner = comparison.CalculateWinner(competitors[i], competitors[j]);
                    winners.Add(winner);
                }
            }

            // And one more factory, of course. Anyone surprised at this point? ;-)
            return ScoreFactory.Create(winners);
        }
    }
}
