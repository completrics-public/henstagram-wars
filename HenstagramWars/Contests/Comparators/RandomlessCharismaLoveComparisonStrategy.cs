﻿using HenstagramWars.Competitors;
using System;
using System.Collections.Generic;
using System.Text;

namespace HenstagramWars.Contests.Comparators
{
    public class RandomlessCharismaLoveComparisonStrategy : IComparisonStrategy
    {
        public Competitor CalculateWinner(Competitor competitor1, Competitor competitor2)
        {
            int score1 = competitor1.Charisma;
            int score2 = competitor2.Charisma;

            if (score1 >= score2) return competitor1;
            else return competitor2;

        }
    }
}
