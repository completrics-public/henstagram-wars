﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace HenstagramWars.Tools
{
    /// <summary>
    /// This class is used for reading files and directories, to make it slightly faster (for a dev) than usual.
    /// Comes from a different project, thus is not optimized for Henstagram Wars.
    /// </summary>
    public static class FileOps
    {
        public static FileRecord[] ReadFiles(string directoryPath)
        {
            string[] filePaths = Directory.GetFiles(directoryPath);
            var fileContents = filePaths.Select(s => ReadFile(s));
            return fileContents.ToArray();
        }

        public static FileRecord ReadFile(string filePath)
        {
            string content = string.Empty;
            using (StreamReader reader = new StreamReader(filePath, Encoding.UTF8))
            {
                content = reader.ReadToEnd();
            }
            return new FileRecord(path: filePath, content: content);
        }
    }

    /// <summary>
    /// FileRecord is a class representing a read file: identifier with a filePath.
    /// 
    /// Comes from a different project, thus is not optimized for Henstagram Wars.
    /// </summary>
    public class FileRecord
    {
        public FileRecord(string path, string content) { Path = path; Content = content; }

        public string Path { get; }
        public string Content { get; }
    }
}
