Gladiators are passe.

Today you cannot be ANYONE IMPORTANT, unless you compete on Henstagram - a very special image service for people possessing chickens, hens and poultry of any kind.

The 'Gladiators' will compete on their hens' attractiveness (Hen), their henhouses' epicness (Henhouse) and their natural charm and political connections (Charisma). Who will become the Master of Henstagram?

This application is designed to show how Factory pattern and Strategy pattern interact and why it can be quite nice. It is written to balance readability with power; wherever those two were in conflict, readability usually won.

This application is, in general, pointless. Just a demo ;-).
