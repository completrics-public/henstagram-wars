﻿using HenstagramWars.Competitors;
using HenstagramWars.Competitors.Schemas;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace HenstagramWarsTest.CompetitorConstruction
{
    public class TestEnumerationSchemaParsers
    {
        [Test]
        public void ProperlyParseValidRecord()
        {
            // Given

            string michael = @"My name is: Michael Junior Rook vel Darnauris
I am a: silent man; don't like to talk much about myself
My henhouse is: pretty, exhaustive.
My prized hen is: white-feathered, full of compassion, always happy.

This is my story.";

            // When

            Competitor actual = new EnumerationSchemaParsingStrategy().Parse(michael);

            // Then

            Assert.AreEqual(actual.Name, "Michael Junior Rook vel Darnauris");
            Assert.IsTrue(actual.Charisma == 1);
            Assert.IsTrue(actual.HenHouse == 2);
            Assert.IsTrue(actual.HenQuality == 3);
        }

        [Test]
        public void ProperlyParseMinimumValidRecord()
        {

            // Given

            string michael = @"My name is: Michael Junior Rook vel Darnauris";

            // When

            Competitor actual = new EnumerationSchemaParsingStrategy().Parse(michael);

            // Then

            Assert.AreEqual(actual.Name, "Michael Junior Rook vel Darnauris");
            Assert.IsTrue(actual.Charisma == 0);
            Assert.IsTrue(actual.HenHouse == 0);
            Assert.IsTrue(actual.HenQuality == 0);
        }

        [Test]
        public void CalculateFitnessForMinimumValidRecord()
        {

            // Given

            string michael = @"My name is: Michael Junior Rook vel Darnauris";

            // When

            double fitness = new EnumerationSchemaParsingStrategy().CalculateFitness(michael);

            // Then

            Assert.AreEqual(0.25, fitness);
        }

        [Test]
        public void CalculateFitnessForInvalidRecord()
        {

            // Given

            string michael = @"asdasdsads";

            // When

            double fitness = new EnumerationSchemaParsingStrategy().CalculateFitness(michael);

            // Then

            Assert.AreEqual(0, fitness);
        }

        [Test]
        public void CalculateFitnessForPerfectRecord()
        {
            // Given

            string michael = @"My name is: Michael Junior Rook vel Darnauris
I am a: silent man; don't like to talk much about myself
My henhouse is: pretty, exhaustive.
My prized hen is: white-feathered, full of compassion, always happy.

This is my story.";

            double fitness = new EnumerationSchemaParsingStrategy().CalculateFitness(michael);

            // Then

            Assert.AreEqual(1, fitness);
        }
    }
}
