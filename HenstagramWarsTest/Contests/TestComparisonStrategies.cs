﻿using HenstagramWars.Competitors;
using HenstagramWars.Contests;
using HenstagramWars.Contests.Comparators;
using HenstagramWars.Scores;
using NUnit.Framework;
using System.Collections.Generic;

namespace HenstagramWarsTest
{
    public class TestComparisonStrategies
    {
        [Test]
        public void RSCS_SelectsHigherSum_For2Competitors()
        {
            // Given
            List<Competitor> competitors = new List<Competitor>
            {
                new Competitor("Adam", 4,3,4),  //sum -> 11
                new Competitor("Barbara", 1,4,5)   //sum -> 10
            };

            // When
            Competitor winner = new RandomlessSumComparisonStrategy().CalculateWinner(competitors[0], competitors[1]);

            // Then
            Assert.IsTrue(winner.Name == "Adam");

        }

        [Test]
        public void RCLCS_SelectsHigherCharisma_For2Competitors()
        {
            // Given
            List<Competitor> competitors = new List<Competitor>
            {
                new Competitor("Adam", 9999,9999,charisma:4), // epic hen and henhouse.
                new Competitor("Barbara", 0,0,charisma:5)     // no hen, no henhouse - but higher charisma
            };

            // When
            Competitor winner = new RandomlessCharismaLoveComparisonStrategy().
                CalculateWinner(competitors[0], competitors[1]);

            // Then
            Assert.IsTrue(winner.Name == "Barbara");

        }
    }
}
